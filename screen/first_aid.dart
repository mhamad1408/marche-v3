import 'package:flutter/material.dart';

class FirstAid extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
  body: ListView(
  scrollDirection: Axis.vertical,
  children: <Widget>[
   SizedBox(
  height: 10,
  ),
  Container(
  color: Colors.yellow,
  child: Center(
  child: Text('First Aid',
   style: TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    ),
  ),
  ),
  ),
  Container(
  height: 150.0,
  width: double.infinity,
  child: SingleChildScrollView(
  scrollDirection: Axis.horizontal,
  child: Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: <Widget>[
   Container(
            height: 120.0,
            width: 120.0,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/a2.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Alcohol',
            style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
  ],
  ),
  ),
  ),
  Container(
    margin: EdgeInsets.symmetric(vertical: 10.0),
    padding:EdgeInsets.all(10),
    height:40,
   child:Text('Alcohol',textAlign: TextAlign.left,style: TextStyle(color:Colors.orange,fontSize: 20,fontWeight: FontWeight.w800)
   ),   
  ),
  Stack(
  children: <Widget>[
  Container(
   padding: EdgeInsets.all(10),
   margin: EdgeInsets.all(10),
   child: SingleChildScrollView(
   scrollDirection: Axis.horizontal,
   child:Row(
   mainAxisAlignment: MainAxisAlignment.spaceBetween,
   children: <Widget>[
     InkWell(
      child: Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      alignment: Alignment.topCenter,
      child: Column(
      children: <Widget>[
      Image.asset('assets/images/a2.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
       ],
       ),
     ),
           onTap: (){},
        ),
   ],
   ),
   ),
  ),
  ],
  ),
  ],
  ),
  );
  
  
}