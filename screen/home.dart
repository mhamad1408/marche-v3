// ignore: import_of_legacy_library_into_null_safe
//import 'package:carousel_pro/carousel_pro.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:scartp/screen/arrivalls.dart';
import 'package:scartp/screen/brands.dart';
import 'package:scartp/screen/mydrawer.dart';
import 'package:scartp/screen/offers.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var country;
   getPref() async{
  SharedPreferences preferences = await SharedPreferences.getInstance();
   country = preferences.getString('country');
   print(country);
  }
   @override
   void initState(){
     getPref();
     super.initState();
   }
  MyArticles (String imageVal,String heading,String subHeading,){
    return Container (
     width:150,
     child:new Row(
     mainAxisAlignment: MainAxisAlignment.spaceBetween,
     children:<Widget> [
      Expanded(
      child: Wrap(
       children:<Widget>[
         Image.asset(imageVal,height: 100,width: 200,fit: BoxFit.fitWidth),
         Directionality(
         textDirection: TextDirection.ltr,
         child:ListTile(
           leading:Wrap(
          children: <Widget>[
           Container(
            child:Text(heading,textAlign:TextAlign.center,style: TextStyle(fontSize:16,fontWeight: FontWeight.w600),), 
           ),
           Container(
            child:Text(subHeading,textAlign:TextAlign.center,style: TextStyle(color:Colors.blue),),
           ),
          ],
           ),
         ) ,
         ),
       ],
       ),
      ),
     ],
     ),
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
       theme: ThemeData(
        primaryColor: Colors.white,
       dividerColor: Colors.black,
      ),
      title: 'Marche',
      home: Scaffold(
        appBar:AppBar(
          title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/images/logo png.png', fit:BoxFit.cover,height: 100,),
          ],
          ),
          centerTitle: true,
          elevation: 6,
          actions: <Widget>[
          IconButton(icon: Icon(Icons.search),color: Colors.red,onPressed: (){
           showSearch(context: context, delegate: SearchData());
          }),
          ],
          ) ,
          //drawer:MyDrawer() ,
        body: ListView(
        scrollDirection: Axis.vertical,
        children:<Widget>[
        SizedBox(
        height: 2,
        ),
         Container(
         //padding: EdgeInsets.all(10),
            height: 200.0,
            width: double.infinity,
            child: Carousel(
              images: [
               Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
               Image.asset('assets/images/b1.jpg', fit: BoxFit.cover,),
               Image.asset('assets/images/m1.png', fit: BoxFit.cover,),
               Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
               Image.asset('assets/images/w2.jpg', fit: BoxFit.cover,),
               Image.asset('assets/images/w3.jpg', fit: BoxFit.cover,),
            ],

            dotSpacing: 10,
            dotSize :1.5,
            //dotIncreaseSize :0.5,
            dotColor: Colors.white,
            //dotBgColor: Colors.blue.withOpacity(0.5),
            //overlayShadow: true,
            //overlayShadowColors: Colors.blue,
            ),
         ),
        //   // End Carsouel
        //Start For Categories Item
        Container(
          height: 150.0,
          width: double.infinity,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
            //Tv & Appliances
            Container(
            height: 100.0,
            width: 106.0,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/t1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('TVs & Appliances',
            style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
            //end Tv & Appliances
            //Bakery
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/b1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Bakery',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
            //End Bakery
            //First Aid
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/f1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('First Aid',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/f1.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End First Aid
            //Deli
            Container(
            height:  100.0,
            width:  100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/D1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Deli',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/D1.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Deli
            //Dairy & Eggs
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/e1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Dairy & Eggs',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/e1.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Dairy & Eggs
            //Cans & Jars
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/c1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Cans & Jars',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/c1.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Cans & Jars
            //Pantry 
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/p1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Pantry',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/p1.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Pantry 
             //Healthy Products 
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/h1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Healthy Products ',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/h1.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Healthy Products 
            //Breakfast food   
            Container(
            height: 100.0,
            width: 106.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/b2.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Breakfast Food',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/b2.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Breakfast food 
            //Chocloate & Snacks 
            Container(
            height: 100.0,
            width: 110.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/c2.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Chocloate & Snacks',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 100,
            // // backgroundImage: AssetImage('assets/images/c2.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Chocloate & Snacks  
            //Frozen & Chill 
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/f2.png',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Frozen & Chilled',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/f2.png',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Frozen & Chilled   
            //Beverages & Juices with no images
            Container(
            height: 100.0,
            width: 110.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/j1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Beverages & Juices',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/j1.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Beverages & Juices 
             // Wine & Alcoohl with no images
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/al1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Wine & Alcoohl',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/al1.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Wine & Alcoohl   with no images
          // Tobacco with no images
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/t2.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Tobacco',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/t2.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Tobacco  with no images
            ],
            ),
          ),
        ),
        Container(
          height: 140.0,
          width: double.infinity,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
            //Mobile
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/m1.png',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Mobiles & Accessories',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/m1.png',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //end Mobile

            //Kitchen
             Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/k1.jpg',),
            ),
            subtitle: Text('Kitchen & Dining',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
            //End Kitchen
            //Beauty
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/b3.jpg',),
            ),
            subtitle: Text('Beauty Boutique',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
            //End Beauty
            //Hair care
            Container(
            height:  100.0,
            width:  100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/h2.jpg',),
            ),
            subtitle: Text('Hair care',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
            //End Hair care
            //Personal care
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/p2.png',),
            ),
            subtitle: Text('Personal care',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
            //End Personal care
            //Cleaning
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/h3.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Cleaning & Home Care',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/h3.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Cleaning
            //Household 
            Container(
            height: 100.0,
            width: 102.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/h4.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Household',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/h4.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Household 
             //Electronics
            Container(
            height: 100.0,
            width: 106.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/e2.jpeg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Electronics',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/e2.jpeg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Electronics
            //Fashion  
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/f3.png',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Fashion',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/f3.png',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Fashion
            //Automotive
            Container(
            height: 100.0,
            width: 104.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/a1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Automotive',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            // child: CircleAvatar(
            //  radius: 40,
            //  backgroundImage: AssetImage('assets/images/a1.jpg',),
            //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
            // ),
            ),
            //End Automotive
            //Kids 
            Container(
            height: 100.0,
            width: 100.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
             child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/k2.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Kids',
             style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
            //End Kids 
            ],
            ),
          ),
        ),
        //End Categories
     //New Arrivalls
      Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
         Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            padding:EdgeInsets.all(10),
            height:50,
      child:Text('New Arrivals',textAlign: TextAlign.left,style: TextStyle(color:Colors.orange,fontSize: 20,fontWeight: FontWeight.w800)
      ),   
     ),
       Container(
        margin: EdgeInsets.only(right:10),
        padding:EdgeInsets.all(10),

        //height:40,
        //width: 30,
        decoration: BoxDecoration(
        border: Border.all(color:Colors.black,),
        ),
       child:InkWell(
        child: Text('VIEW ALL',style: TextStyle(color:Colors.blue,fontSize:10,),
         ),
        onTap: (){
              Navigator.push(context,MaterialPageRoute(builder:(context)=>Arrivalls()));
             },
       ),
     ),
    ],
     ),
          Stack(children:<Widget>[
            Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: SingleChildScrollView(
             scrollDirection: Axis.horizontal,
             child:Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
            InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
                child: Column(
                children: <Widget>[
                Image.asset('assets/images/picon.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                 SizedBox(
                height: 5,
                ),
                Text('Picon Simply 140g',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('Picon Simply Cheese Jars 140GR',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('9,500 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),               
                ],
              ),
              //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
          ),
           onTap: (){},
        ),
        //End one new Arrival
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/Stik.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                SizedBox(
                height: 5,
                ),
                Text('Chio Stickletti Orginal 40g',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('Chio Stickletti Orginal 40g',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('6.500 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end two
            InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/cookie.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                SizedBox(
                height: 5,
                ),
                Text('Nestle Cookie Crisp 375G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 5,
                ),
                 Text('Nestle Cookie Crisp "Chocolaty Chip Cookie Cereal"375G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 4,
                ),
                Text('72,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
               
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        //End Three
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/strawberry.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                SizedBox(
                height: 5,
                ),
                Text('Nestle Curiously Strawberry 450G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('Nestle Curiously Strawberry 450G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('79,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //End For
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/kellog.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                SizedBox(
                height: 2,
                ),
                Text('Kellogg Red Berries 330G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 1,
                ),
                 Text('Kellogg Special K Red Berries Multigrain Flakes With Burst Of Strawberries',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color:Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 1,
                ),
                Text('95,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //End Five
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/jordans.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                SizedBox(
                height: 5,
                ),
                Text('Jordans Country Crisp Choco 500G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                 Text('Jordans Country Crisp With 70%  Cocoa Dark Chocolate 500G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('95,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //End Sex
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/green.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                SizedBox(
                height: 5,
                ),
                Text('Green Giant Sweet Corn 198G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 5,
                ),
                Text('Green Giant No Added Suger Sweet Corn 198G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color:Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('4.750 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //End Seven
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
             //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/fox.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                SizedBox(
                height: 5,
                ),
                Text('Fox Biscuit 275G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                  Text('Fox Fabulous Biscuit Selection Box 275G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color:Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('110,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //End Eghit
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/tropical.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                SizedBox( 
                height: 5,
                ),
                Text('El SADA Tropical Blossoms Shower Gel 750Ml',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                // SizedBox( 
                // height: 5,
                // ),
                  Text('El SADA Tropical Blossoms Shower Gel 750Ml',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color:Colors.black54),textAlign: TextAlign.center,
                ),
                // SizedBox(
                // height: 4,
                // ),
                Text('28,620 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //End Nine
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/sada.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                SizedBox(
                height: 5,
                ),
                Text('El SADA Exotic Hibiscus  Shower Gel 750Ml',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                // SizedBox(
                // height: 5,
                // ),
                 Text('El SADA Exotic Hibiscus  Shower Gel 750Ml',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color:Colors.black54),textAlign: TextAlign.center,
                ),
                // SizedBox(
                // height: 10,
                // ),
                Text('28,620 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //End Ten
      ],
      ),
       ),
     ),
        ],
       ),
     //Special Offers
         Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
         Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            padding:EdgeInsets.all(10),
            height:50,
      child:Text('Special Offers',textAlign: TextAlign.left,style: TextStyle(color:Colors.orange,fontSize: 20,fontWeight: FontWeight.w800)
      ),   
     ),
         Container(
        margin: EdgeInsets.only(right:10),
        padding:EdgeInsets.all(10),

        //height:40,
        //width: 30,
        decoration: BoxDecoration(
        border: Border.all(color:Colors.black,),
        ),
       child:InkWell(
        child: Text('VIEW ALL',style: TextStyle(color:Colors.blue,fontSize:10,),
         ),
        onTap: (){
              Navigator.push(context,MaterialPageRoute(builder:(context)=>Arrivalls()));
             },
       ),
     ),
    ],
     ),
          Stack(children:<Widget>[
           Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: SingleChildScrollView(
             scrollDirection: Axis.horizontal,
             child:Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
        //here update
            InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/smdes.jpg',fit:BoxFit.cover,height: 100,width: 80,),
                SizedBox(
                height: 5,
                ),
                Text('Smdes Cheese 8p',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                 Text('Smdes Cheese 8 Pieces',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color:Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('8,500 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //Start update
           InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/kiri.jpg',fit:BoxFit.cover,height: 100,width: 80,),
                SizedBox(
                height: 5,
                ),
                Text('Kiri Cheese 6p',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                 Text('Kiri Cheese Spread 6 Poritions',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('20,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //Start update
          InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/dragon.jpg',fit:BoxFit.cover,height: 100,width: 80,),
                SizedBox(
                height: 5,
                ),
                Text('Choco Dragon Shells',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                 Text('Kellogg Zimmys Choco Dragon Shells 450g',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('65,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //Start Update
           InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/regilait.jpg',fit:BoxFit.cover,height: 100,width: 80,),
                SizedBox(
                height: 5,
                ),
                Text('Regilait Fat Free 400G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                 Text('Regilait Instant Skimmed Milk Powder 0% 400g',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('70,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //Start update
            InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/beef.jpg',fit:BoxFit.cover,height: 100,width: 80,),
                SizedBox(
                height: 5,
                ),
                Text('Daily Beef Burger 800G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('Special Offer Daily Beef Burger 800G + Fries 900g',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('69,500 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //Start Update
          InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/kiri.jpg',fit:BoxFit.cover,height: 100,width: 80,),
                SizedBox(
                height: 5,
                ),
                Text('Kiri Cheese 24p',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('73,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //Start update
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/stax.jpg',fit:BoxFit.cover,height: 100,width: 80,),
                SizedBox(
                height: 5,
                ),
                Text('Lays Stax Sour Crean & Onion 2x156G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                 Text('Lays Stax Sour Crean & Onion 2x156G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('55,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //Start update
         //Start update
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/cucina.jpg',fit:BoxFit.cover,height: 100,width: 80,),
                SizedBox(
                height: 5,
                ),
                Text('Cucina Doro cream 1L',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('Cucina Doro cream 1L',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('47,520 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //Start update
              InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/nag.jpg',fit:BoxFit.cover,height: 100,width: 80,),
                SizedBox(
                height: 5,
                ),
                Text('Daily Chicken Nuggets',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                 Text('Daily Chicken Nuggets',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('51,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
        //Statr update
            InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
             // color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
             child: Column(
                children: <Widget>[
                Image.asset('assets/images/burger.png',fit:BoxFit.cover,height: 100,width: 80,),
                SizedBox(
                height: 5,
                ),
                Text('Daily Chicken Burger 800G',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('Daily Chicken Burger 800G + Fries 900g',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('52,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue), 
                ],
              ),
          ),
           onTap: (){},
        ),
      ],
      ),
      ),
     ),
    ],
   ),
           //here Brand Start
        Container(
          height: 80,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: <Widget>[
          Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              padding:EdgeInsets.all(10),
              height:40,
             child:Text('Brands',textAlign: TextAlign.left,style: TextStyle(color:Colors.orange,fontSize: 20,fontWeight: FontWeight.w800)
      ),   
     ),
      Container(
        margin: EdgeInsets.only(right:10),
        padding:EdgeInsets.all(10),

        //height:40,
        //width: 30,
        decoration: BoxDecoration(
        border: Border.all(color:Colors.black,),
        ),
       child:InkWell(
        child: Text('VIEW ALL',style: TextStyle(color:Colors.blue,fontSize:10,),
         ),
        onTap: (){
              Navigator.push(context,MaterialPageRoute(builder:(context)=>Brands()));
             },
       ),
     ),
    ],
     ),
    ),
          Stack(
          children:<Widget>[
           Container(
            padding: EdgeInsets.all(5),
            margin: EdgeInsets.all(5),
            child: SingleChildScrollView(
             scrollDirection: Axis.horizontal,
             child:Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
            InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:100,
              // decoration: BoxDecoration(
              // borderRadius: BorderRadius.all(Radius.zero),
              // border: Border.all(color:Colors.black,) 
              // ),
              child:Card(
                child: Image.asset('assets/images/v1.png',fit:BoxFit.cover,height: 100,width: 200,),
              ),
          ),
          onTap: (){},
            ),
            InkWell(
            child: Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.all(5),
              alignment: Alignment.topCenter,
              width:150,
              height:90,
              //  decoration: BoxDecoration(
              // borderRadius: BorderRadius.all(Radius.zero),
              // border: Border.all(color:Colors.black,) 
              // ),
              child: Card(
                child: Image.asset('assets/images/v2.jpg',fit:BoxFit.cover,height: 80,width: 150,),
              ),
          ),
              onTap: (){},
            ),
            InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
               height:100,
              //  decoration: BoxDecoration(
              // borderRadius: BorderRadius.all(Radius.zero),
              // border: Border.all(color:Colors.black,) 
              // ),
              child: Card(
                child: Image.asset('assets/images/s1.png',fit:BoxFit.cover,height: 100,width: 200,),
              ),
          ),
           onTap: (){},
            ),
             InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
               height:100,
              //  decoration: BoxDecoration(
              // borderRadius: BorderRadius.all(Radius.zero),
              // border: Border.all(color:Colors.black,) 
              // ),
              child: Card(
                child: Image.asset('assets/images/j2.jpg',fit:BoxFit.cover,height: 100,width: 200,),
              ),
          ),
           onTap: (){},
            ),
             InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
               height:100,
              //  decoration: BoxDecoration(
              // borderRadius: BorderRadius.all(Radius.zero),
              // border: Border.all(color:Colors.black,) 
              // ),
              child: Card(
                child: Image.asset('assets/images/p3.jpg',fit:BoxFit.cover,height: 100,width: 200,),
              ),
          ),
           onTap: (){},
            ),
             InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
               height:100,
              //  decoration: BoxDecoration(
              // borderRadius: BorderRadius.all(Radius.zero),
              // border: Border.all(color:Colors.black,) 
              // ),
              child: Card(
                child: Image.asset('assets/images/e3.jpg',fit:BoxFit.cover,height: 100,width: 200,),
              ),
          ),
           onTap: (){},
            ),
             InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
               height:100,
              //  decoration: BoxDecoration(
              // borderRadius: BorderRadius.all(Radius.zero),
              // border: Border.all(color:Colors.black,) 
              // ),
              child: Card(
                child: Image.asset('assets/images/l1.jpg',fit:BoxFit.cover,height: 100,width: 200,),
              ),
          ),
           onTap: (){},
            ),
             InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
               height:100,
              //  decoration: BoxDecoration(
              // borderRadius: BorderRadius.all(Radius.zero),
              // border: Border.all(color:Colors.black,) 
              // ),
              child: Card(
                child: Image.asset('assets/images/el1.jpg',fit:BoxFit.cover,height: 100,width: 200,),
              ),
          ),
           onTap: (){},
            ),
             InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
               height:100,
              //  decoration: BoxDecoration(
              // borderRadius: BorderRadius.all(Radius.zero),
              // border: Border.all(color:Colors.black,) 
              // ),
              child: Card(
                child: Image.asset('assets/images/d2.png',fit:BoxFit.cover,height: 100,width: 200,),
              ),
          ),
           onTap: (){},
            ),
             InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
               height:100,
              //  decoration: BoxDecoration(
              // borderRadius: BorderRadius.all(Radius.zero),
              // border: Border.all(color:Colors.black,) 
              // ),
              child: Card(
                child: Image.asset('assets/images/zw.png',fit:BoxFit.cover,height: 100,width: 200,),
              ),
          ),
           onTap: (){},
            ),
      ],
      ),
       ),
     ),
       ],
    ),
   ],
   ),
      ),
    );
  }
}


class SearchData extends SearchDelegate<String>{
  @override
  List<Widget> buildActions(BuildContext context) {
      //  buildActions
      return [
        IconButton(icon: Icon(Icons.clear), onPressed: (){

        }),
      ];
    }
  
    @override
    Widget buildLeading(BuildContext context) {
      //  buildLeading
      return IconButton(
      icon: Icon(Icons.arrow_back), onPressed: (){

        }
      );
    }
  
    @override
    Widget buildResults(BuildContext context) {
      //  buildResults
      return null;
    }
  
    @override
    Widget buildSuggestions(BuildContext context) {
    // TODO: buildSuggestions
    return Text('Search');
    
  }

}