import 'package:flutter/material.dart';

class Bakery extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
  body: ListView(
  children: <Widget>[
   SizedBox(
  height: 10,
  ),
  Container(
  color: Colors.teal,
  child: Center(
  child: Text('Bakery',
   style: TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    ),
  ),
  ),
  ),
    Container(
   height: 150.0,
   width: double.infinity,
  child: SingleChildScrollView(
  scrollDirection: Axis.horizontal,
  child: Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: <Widget>[
   Container(
            height: 120.0,
            width: 120.0,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/ba.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Buns & Samdwiches',
            style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
             Container(
            height: 120.0,
            width: 120.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/pa.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Pastry & Savory',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
             Container(
            height: 120.0,
            width: 120.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/ka.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('KaaK',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
             Container(
            height: 120.0,
            width: 120.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/ar.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Arbic Bread',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
    Container(
            height: 120.0,
            width: 120.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/gh.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Ghraybe',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
            Container(
            height: 120.0,
            width: 120.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/to.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Toasts & Crisps',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
              Container(
            height: 120.0,
            width: 120.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/fr1.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Freshly Baked',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
  ],
  ),
  ),
  ),
  ],
  ),
  );
  
  
}


  // Container(
  // height: 80,
  // child: Row(
  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
  // children: <Widget>[
  //      Container(
  //             margin: EdgeInsets.symmetric(vertical: 10.0),
  //             padding:EdgeInsets.all(10),
  //             height:40,
  //            child:Text('Buns & Samdwiches',textAlign: TextAlign.left,style: TextStyle(color:Colors.orange,fontSize: 20,fontWeight: FontWeight.w800)
  //     ),   
  //    ),
  // ],
  // ),
  // ),
  // Stack(
  // children: <Widget>[
  // Container(
  //   padding: EdgeInsets.all(10),
  //   margin: EdgeInsets.all(10),
  //   child: SingleChildScrollView(
  //   scrollDirection: Axis.horizontal,
  //   child:Row(
  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //   children: <Widget>[
  //    InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t40.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t50.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t51.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t52.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t53.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t54.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t56.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t57.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t58.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t60.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t65.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t70.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //   ],
  //   ),
  //   ),
  // ),

  // ],
  // ),
  // Container(
  //             margin: EdgeInsets.symmetric(vertical: 10.0),
  //             padding:EdgeInsets.all(10),
  //             height:40,
  //            child:Text('Pastry & Savory',textAlign: TextAlign.left,style: TextStyle(color:Colors.orange,fontSize: 20,fontWeight: FontWeight.w800)
  //     ),   
  //    ),
  //    Stack(
  // children: <Widget>[
  // Container(
  //   padding: EdgeInsets.all(10),
  //   margin: EdgeInsets.all(10),
  //   child: SingleChildScrollView(
  //   scrollDirection: Axis.horizontal,
  //   child:Row(
  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //   children: <Widget>[
  //    InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t40.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t50.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t51.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t52.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t53.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t54.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t56.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t57.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t58.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t60.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t65.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t70.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //   ],
  //   ),
  //   ),
  // ),
  // ],
  // ),
  //   Container(
  //   margin: EdgeInsets.symmetric(vertical: 10.0),
  //   padding:EdgeInsets.all(10),
  //   height:40,
  //  child:Text('KaaK',textAlign: TextAlign.left,style: TextStyle(color:Colors.orange,fontSize: 20,fontWeight: FontWeight.w800)
  //  ),   
  // ),
  // Stack(
  // children: <Widget>[
  // Container(
  //   padding: EdgeInsets.all(10),
  //   margin: EdgeInsets.all(10),
  //   child: SingleChildScrollView(
  //   scrollDirection: Axis.horizontal,
  //   child:Row(
  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //   children: <Widget>[
  //    InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t40.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t50.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t51.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t52.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t53.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t54.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t56.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t57.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t58.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t60.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t65.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t70.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //   ],
  //   ),
  //   ),
  // ),
  // ],
  // ),
  //  Container(
  //   margin: EdgeInsets.symmetric(vertical: 10.0),
  //   padding:EdgeInsets.all(10),
  //   height:40,
  //  child:Text('Arabic Bread',textAlign: TextAlign.left,style: TextStyle(color:Colors.orange,fontSize: 20,fontWeight: FontWeight.w800)
  //  ),   
  // ),
  // Stack(
  // children: <Widget>[
  // Container(
  //   padding: EdgeInsets.all(10),
  //   margin: EdgeInsets.all(10),
  //   child: SingleChildScrollView(
  //   scrollDirection: Axis.horizontal,
  //   child:Row(
  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //   children: <Widget>[
  //    InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t40.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t50.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t51.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t52.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t53.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t54.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t56.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t57.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t58.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t60.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t65.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t70.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //   ],
  //   ),
  //   ),
  // ),
  // ],
  // ),
  //   Container(
  //   margin: EdgeInsets.symmetric(vertical: 10.0),
  //   padding:EdgeInsets.all(10),
  //   height:40,
  //  child:Text('Ghraybe',textAlign: TextAlign.left,style: TextStyle(color:Colors.orange,fontSize: 20,fontWeight: FontWeight.w800)
  //  ),   
  // ),
  // Stack(
  // children: <Widget>[
  // Container(
  //   padding: EdgeInsets.all(10),
  //   margin: EdgeInsets.all(10),
  //   child: SingleChildScrollView(
  //   scrollDirection: Axis.horizontal,
  //   child:Row(
  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //   children: <Widget>[
  //    InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t40.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t50.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t51.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t52.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t53.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t54.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t56.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t57.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t58.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t60.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t65.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t70.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //   ],
  //   ),
  //   ),
  // ),
  // ],
  // ),
  // Container(
  //   margin: EdgeInsets.symmetric(vertical: 10.0),
  //   padding:EdgeInsets.all(10),
  //   height:40,
  //  child:Text('Toasts & Crips',textAlign: TextAlign.left,style: TextStyle(color:Colors.orange,fontSize: 20,fontWeight: FontWeight.w800)
  //  ),   
  // ),
  // Stack(
  // children: <Widget>[
  // Container(
  //   padding: EdgeInsets.all(10),
  //   margin: EdgeInsets.all(10),
  //   child: SingleChildScrollView(
  //   scrollDirection: Axis.horizontal,
  //   child:Row(
  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //   children: <Widget>[
  //    InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t40.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t50.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t51.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t52.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t53.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t54.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t56.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t57.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t58.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t60.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t65.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t70.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //   ],
  //   ),
  //   ),
  // ),
  // ],
  // ),
  // Container(
  //   margin: EdgeInsets.symmetric(vertical: 10.0),
  //   padding:EdgeInsets.all(10),
  //   height:40,
  //  child:Text('Freshly Baked',textAlign: TextAlign.left,style: TextStyle(color:Colors.orange,fontSize: 20,fontWeight: FontWeight.w800)
  //  ),   
  // ),
  // Stack(
  // children: <Widget>[
  // Container(
  //   padding: EdgeInsets.all(10),
  //   margin: EdgeInsets.all(10),
  //   child: SingleChildScrollView(
  //   scrollDirection: Axis.horizontal,
  //   child:Row(
  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //   children: <Widget>[
  //    InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t40.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t50.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t51.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //         InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t52.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t53.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t54.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t56.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t57.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t58.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t60.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t65.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //        InkWell(
  //             child: Container(
  //             padding: EdgeInsets.all(10),
  //             margin: EdgeInsets.all(10),
  //             alignment: Alignment.topCenter,
  //               child: Column(
  //               children: <Widget>[
  //               Image.asset('assets/images/t70.jpg',fit:BoxFit.cover,height: 100,width: 50,),       
  //               ],
  //             ),
  //         ),
  //          onTap: (){},
  //       ),
  //   ],
  //   ),
  //   ),
  // ),
  // ],
  // ),