import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:scartp/model/cart.dart';
import 'package:scartp/model/item.dart';
import 'package:scartp/screen/CheckoutScreen.dart';
import 'package:scartp/screen/details.dart';

class Apple extends StatefulWidget {
  @override
  _AppleState createState() => _AppleState();
}

class _AppleState extends State<Apple> {
    final List<Item> items = [
    Item(title: 'laptop ', price: 500.0,img:'assets/images/w17.jpg'),
    Item(title: 'iphone x ', price: 400.0,img:'assets/images/w17.jpg'),
    Item(title: 'keyboard ', price: 40.0,img:'assets/images/w17.jpg'),
  ];
  @override
  Widget build(BuildContext context) {
      return Consumer<Cart>(builder: (context, cart, child) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Shopping cart'),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => CheckoutPage()));
                    },
                  ),
                  Text(cart.count.toString())
                ],
              ),
            )
          ],
          centerTitle: true,
        ),
        body: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, index) {
            return InkWell(
            child: Container(
            height: 100,
            width: 100,
            child: Card(
            child: Row(
            children:<Widget> [
            Expanded(flex: 1,
            child:   Image.asset('assets/images/w17.jpg'),
             ),
             Expanded(flex: 2,
             child: Container(
             height: 100,
            alignment: Alignment.center,
            child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
            Text(items[index].title,style:TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                //  Expanded(
                //    child: Text('Price:',),
                //  ),
                   Expanded(
                child: Text(items[index].price.toString(),style: TextStyle(color: Colors.red),),
                ),
                InkWell(child: Icon(Icons.add,
                ),
                  onTap: () {
                    cart.add(items[index]);
                  // Navigator.push(context,MaterialPageRoute(builder:(context)=>Details()));
                },
                ),
                // InkWell(
                //  child: Text('More Details',style: TextStyle(color: Colors.blue),),
                //  onTap: (){
                //     Navigator.push(context,MaterialPageRoute(builder:(context)=>Details()));
                //  },
                // ),

              ],
            ),
            Container(
            height: 20,
            child:InkWell(
             child: Text('More Details',style: TextStyle(color: Colors.blue),),
              onTap: (){
                  Navigator.push(context,MaterialPageRoute(builder:(context)=>Details()));
              },
            ),
            ),
            ],
            ),
             ),
             ),
            // ListTile(
            //             title: Image.asset(items[index].img),
            //             leading: Text(items[index].title),
            //             subtitle: Text(items[index].price.toString()),
            //             trailing: Icon(Icons.add),
            //             onTap: () {
            //               cart.add(items[index]);
            //             },
            //          ),
                      ],
                    ),
                  ),
                ),
              onTap: () {
                  cart.add(items[index]);
                },
            );
          },
        ),
      );
    });
  }
}