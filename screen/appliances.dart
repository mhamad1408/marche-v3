import 'package:flutter/material.dart';
import 'package:scartp/screen/home.dart';

class Appliances extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
  appBar: AppBar(
     title: Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
    Image.asset('assets/images/logo png.png', fit:BoxFit.cover,height: 100,),
    ],
    ),
    centerTitle: true,
     elevation: 6,
          actions: <Widget>[
          IconButton(icon: Icon(Icons.search),color: Colors.red,onPressed: (){
           showSearch(context: context, delegate: SearchData());
          }),
          ],
  ),
  body: ListView(
  scrollDirection: Axis.vertical,
  children: <Widget>[
  SizedBox(
  height: 10,
  ),
  Container(
    color: Colors.teal,
    child: Center(
    child: Text('Tv & Appliances',
    style: TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    ),
    ),
  ),
  ),
  Container(
   height: 150.0,
   width: double.infinity,
  child: SingleChildScrollView(
  scrollDirection: Axis.horizontal,
  child: Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: <Widget>[
   Container(
            height: 120.0,
            width: 120.0,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/t23.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Television',
            style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
             Container(
            height: 120.0,
            width: 120.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/h5.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Home Audio',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
             Container(
            height: 120.0,
            width: 120.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/t24.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Tv Accessories',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
             Container(
            height: 120.0,
            width: 120.0,
             padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
            decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            ),
            child: ListTile(
            title: CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage('assets/images/p11.jpg',),
            //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
            ),
            subtitle: Text('Projectors',
             style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
            textAlign: TextAlign.center,
            ),
            ),
            ),
  ],
  ),
  ),
  ),
  //  Container(
  // child: Text('Television',
  //  style: TextStyle(
  //   fontSize: 20,
  //   color: Colors.black,
  //   fontWeight: FontWeight.bold,
  //   ),
  // ),
  // ),
   Container(
  child:SingleChildScrollView(
  scrollDirection: Axis.horizontal,
  child: Row(
   mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: <Widget>[
  InkWell(
  child: Container(
  padding: EdgeInsets.all(10),
  margin: EdgeInsets.all(10),
  alignment: Alignment.topCenter,
   decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
  width:150,
  height:250,
  child: Column(
                  children: <Widget>[
                  Image.asset('assets/images/t40.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                   SizedBox(
                  height: 5,
                  ),
                  Text('Elements 50 Inch Tv',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                  ),
                  SizedBox(
                  height: 5,
                  ),
                  Text('7,955,000 L.L',
                  style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                  ),
                   SizedBox(
                  height: 5,
                  ),
                  Icon(Icons.add_shopping_cart, color:Colors.blue),               
                  ],
                ),
            ),
             onTap: (){},
    ),
     InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
               decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              width:150,
              height:250,
                child: Column(
                children: <Widget>[
                Image.asset('assets/images/t50.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                 SizedBox(
                height: 5,
                ),
                Text('Elements 40 Inch TV Breakless',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('5,375,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),               
                ],
              ),
          ),
           onTap: (){},
        ),
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
               decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              width:150,
              height:250,
                child: Column(
                children: <Widget>[
                Image.asset('assets/images/t51.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                 SizedBox(
                height: 5,
                ),
                Text('Elements 40 Inch TV ',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('5,160,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),               
                ],
              ),
          ),
           onTap: (){},
        ),
    ],
    ),
  ) ,
 ),
  Container(
  child:SingleChildScrollView(
  scrollDirection: Axis.horizontal,
  child: Row(
   mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: <Widget>[
  InkWell(
  child: Container(
  padding: EdgeInsets.all(10),
  margin: EdgeInsets.all(10),
  alignment: Alignment.topCenter,
   decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
  width:150,
  height:250,
  child: Column(
                  children: <Widget>[
                  Image.asset('assets/images/t40.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                   SizedBox(
                  height: 5,
                  ),
                  Text('Elements 24 Inch Tv',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                  ),
                  SizedBox(
                  height: 5,
                  ),
                  Text('2,386,300 L.L',
                  style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                  ),
                   SizedBox(
                  height: 5,
                  ),
                  Icon(Icons.add_shopping_cart, color:Colors.blue),               
                  ],
                ),
            ),
             onTap: (){},
    ),
     InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
               decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
                child: Column(
                children: <Widget>[
                Image.asset('assets/images/t50.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                 SizedBox(
                height: 5,
                ),
                Text('Elements 85 Inch TV ',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('365,375,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),               
                ],
              ),
          ),
           onTap: (){},
        ),
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
               decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
                child: Column(
                children: <Widget>[
                Image.asset('assets/images/t51.jpg',fit:BoxFit.cover,height: 100,width: 50,),
                 SizedBox(
                height: 5,
                ),
                Text('Elements 40 Inch TV ',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('5,160,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),               
                ],
              ),
          ),
           onTap: (){},
        ),
    ],
    ),
  ) ,
 ),
//  Stack(
//  children: <Widget>[
//  Container(
//  padding: EdgeInsets.all(10),
//  margin: EdgeInsets.all(10),
//  child: SingleChildScrollView(
//  scrollDirection: Axis.horizontal,
//  child:Row(
//  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//  children: <Widget>[
//    InkWell(
//    child: Container(
//    padding: EdgeInsets.all(10),
//    margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//               width:150,
//               height:250,
//                 child: Column(
//                 children: <Widget>[
//                 Image.asset('assets/images/t40.jpg',fit:BoxFit.cover,height: 100,width: 50,),
//                  SizedBox(
//                 height: 5,
//                 ),
//                 Text('Elements 50 Inch Tv',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 // Text('Picon Simply Cheese Jars 140GR',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                 // ),
//                 // SizedBox(
//                 // height: 10,
//                 // ),
//                 Text('7,955,000 L.L',
//                 style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
//                 ),
//                  SizedBox(
//                 height: 5,
//                 ),
//                 Icon(Icons.add_shopping_cart, color:Colors.blue),               
//                 ],
//               ),
//               //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
//           ),
//            onTap: (){},
//         ),
//           InkWell(
//               child: Container(
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//               width:150,
//               height:250,
//               // decoration: BoxDecoration(
//               // borderRadius: BorderRadius.all(Radius.zero),
//               // border: Border.all(color:Colors.black,),
//               // //color: Colors.yellowAccent.shade400,
//               // ),
//                 child: Column(
//                 children: <Widget>[
//                 Image.asset('assets/images/t50.jpg',fit:BoxFit.cover,height: 100,width: 50,),
//                  SizedBox(
//                 height: 5,
//                 ),
//                 Text('Elements 40 Inch TV Breakless',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
//                 ),
//                 // SizedBox(
//                 // height: 5,
//                 // ),
//                 // Text('Picon Simply Cheese Jars 140GR',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                 // ),
//                 SizedBox(
//                 height: 10,
//                 ),
//                 Text('9,500 L.L',
//                 style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
//                 ),
//                  SizedBox(
//                 height: 5,
//                 ),
//                 Icon(Icons.add_shopping_cart, color:Colors.blue),               
//                 ],
//               ),
//               //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
//           ),
//            onTap: (){},
//         ),
//  ],
//  ),
//  ),
//  ),
//  ],
//  ),



  ],
  ),
  );
  
  
}