import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:scartp/model/cart.dart';
import 'package:scartp/screen/category.dart';
import 'package:scartp/screen/CheckoutScreen.dart';
import 'package:scartp/screen/categories.dart';
import 'package:scartp/screen/home.dart';
import 'package:scartp/screen/profile.dart';
import 'package:scartp/screen/search.dart';
void main() {
  runApp(ChangeNotifierProvider(
    create: (context) => Cart(),
    child: MyApp(),
  ));
}
class MyApp extends StatelessWidget {
 static final String title = 'User Profile';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.white,
       dividerColor: Colors.black,
      ),
      title: 'Mobtech',
      home:MyHomePage(),
      routes: {
      'Categories' : (context){
        return Categories();
      }
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {
 TabController _tabController;

void initState(){
  super.initState();
  _tabController = TabController(length: 5, vsync: this);
}

void dispose(){
  super.dispose();
  _tabController.dispose();
}
  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      body: TabBarView(
      children: <Widget>[
        Home(),
        Category(),
        Search(),
        ProfilePage(),
        CheckoutPage(),
        ],
          controller: _tabController,
      ),
       bottomNavigationBar: Container(
      // padding: EdgeInsets.all(16.0),
            child: ClipRRect(
        //  borderRadius: BorderRadius.all(Radius.circular(8.0),
        //  ),
            child: Container(
           color: Colors.white70,
            child: TabBar(
             labelColor: Colors.blue,
             unselectedLabelColor: Colors.black54,
             labelStyle: TextStyle(fontSize:8.0),
             indicator: UnderlineTabIndicator(borderSide: BorderSide(color: Colors.red,width: 0.0),
             insets:EdgeInsets.fromLTRB(50, 0, 50, 0),
             ),
             indicatorColor:Colors.yellowAccent,
             tabs: <Widget>[
            Tab(
              icon: Icon(Icons.home, size: 24,),
              child: Text('Home',style: TextStyle(color:Colors.blueAccent)
              ),
            ),
            Tab(
              icon: Icon(Icons.category, size: 24,),
              child: Text('Categories',style: TextStyle(color:Colors.blueAccent)
              ),
            ),
            Tab(
               icon: Icon(Icons.search,size: 24,),
              child: Text('Search',style: TextStyle(color:Colors.blueAccent)
              ),
            ),
            Tab(
               icon: Icon(Icons.person_outline_outlined,size: 24,),
              child: Text('profile',style: TextStyle(color:Colors.blueAccent)
              ),
            ),
             Tab(
               icon: Icon(Icons.add_shopping_cart,size: 24,),
              child: Text('cart',style: TextStyle(color:Colors.blueAccent),
              ),
            ),
           ],
           controller: _tabController,
           ),
         ),
       ),
     ),
    );
  }
}